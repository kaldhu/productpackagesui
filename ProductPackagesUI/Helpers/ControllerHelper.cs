﻿using BusinessLayer;
using BusinessLayer.Managers.Currency;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductPackagesUI.Helpers
{
    public class ControllerHelper: IControllerHelper
    {
        private readonly IProcessWebRequest _processWebRequest;
        private readonly ICurrencyManager _currencyManager;

        public ControllerHelper(IProcessWebRequest processWebRequest,
            ICurrencyManager currencyManager)
        {
            _processWebRequest = processWebRequest;
            _currencyManager = currencyManager;
        }

        public void ConfigureCurrencyInViewBag(dynamic viewBag, string currency)
        {
            if (string.IsNullOrEmpty(currency))
            {
                currency = _currencyManager.GetDefaultCurrency(); ;
            }
            viewBag.CurrentCurrency = currency;
            viewBag.AvailableCurrency = _currencyManager.GetAllAvailableCurrencies();
            viewBag.CurrencySymbol = _currencyManager.GetSymbol(currency);
        }
    }
}
