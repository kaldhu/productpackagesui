﻿namespace ProductPackagesUI.Helpers
{
    public interface IControllerHelper
    {
        void ConfigureCurrencyInViewBag(dynamic viewBag, string currency);
    }
}