﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.Managers.Currency;
using Microsoft.AspNetCore.Mvc;
using ProductPackagesUI.Helpers;
using ProductPackagesUI.Models;

namespace ProductPackagesUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICurrencyManager _currencyManager;
        private readonly IProcessWebRequest _processWebRequest;
        private readonly IControllerHelper _controllerHelper;

        public HomeController(ICurrencyManager currencyManager, IProcessWebRequest processWebRequest,
            IControllerHelper controllerHelper)
        {
            _controllerHelper = controllerHelper;
            _currencyManager = currencyManager;
            _processWebRequest = processWebRequest;
        }

        public IActionResult Index(string currency = null)
        {
            return RedirectToAction("Index", "Packages", new { currency });
        }

        public IActionResult About(string currency = null)
        {
            ViewData["Message"] = "Your application description page.";

            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            return View();
        }

        public IActionResult Contact(string currency = null)
        {
            ViewData["Message"] = "Your contact page.";

            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            return View();
        }

        public IActionResult Privacy(string currency = null)
        {
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
