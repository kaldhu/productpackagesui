﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLayer;
using BusinessLayer.Managers.Currency;
using BusinessLayer.Managers.Product;
using BusinessLayer.Managers.ProductPackage;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using ProductPackagesUI.Helpers;
using ProductPackagesUI.Models;

namespace ProductPackagesUI.Controllers
{
    public class PackagesController : Controller
    {
        private readonly IProcessWebRequest _processWebRequest;
        private readonly ICurrencyManager _currencyManager;
        private readonly IProductPackageManager _productPackageManager;
        private readonly IProductManager _productManager;
        private readonly IServiceProvider _serviceProvider;
        private readonly IControllerHelper _controllerHelper;
        private readonly IConfiguration _configuration;
        
        public PackagesController(
            IConfiguration configuration,
            IServiceProvider serviceProvider,
            IProcessWebRequest processWebRequest,
            ICurrencyManager currencyManager,
            IProductPackageManager productPackageManager,
            IProductManager productManager,
            IControllerHelper controllerHelper)
        {
            _configuration = configuration;
            _controllerHelper = controllerHelper;
            _serviceProvider = serviceProvider;
            _productPackageManager = productPackageManager;
            _processWebRequest = processWebRequest;
            _currencyManager = currencyManager;
            _productManager = productManager;
        }

        #region Index

        public IActionResult Index(string currency = null)
        {
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            var viewModel = new ProductPackageIndexViewModel();

            var productPackages = _productPackageManager.GetAllProductPackages(currency);
            viewModel.ProductPackageViewModels = productPackages.Select(productPackage => new ProductPackageViewModel(productPackage)).ToList();

            return View(viewModel);
        }

        #endregion

        #region Details

        public IActionResult Details(string id, string currency = null)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index", new { currency });
            }
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            var productPackage = _productPackageManager.GetProductPackage(id, currency);
            var viewModel = new ProductPackageDetailsViewModel();
            viewModel.ProductPackageViewModel = new ProductPackageViewModel(productPackage);

            foreach (var product in viewModel.ProductPackageViewModel.Products)
            {
                var productModel = _productManager.GetProductModel(product.Id);
                product.Name = productModel.Name;
                product.Value = _currencyManager.ApplyExchangeRate(productModel.usdPrice, currency);
            }
            return View(viewModel);
        }

        #endregion

        #region Delete
        
        [HttpGet]
        public IActionResult ConfirmDelete(string id, string currency = null)
        {
            var productPackage = _productPackageManager.GetProductPackage(id, currency);
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            var viewModel = new ProductPackageDetailsViewModel();
            viewModel.ProductPackageViewModel = new ProductPackageViewModel(productPackage);
            return PartialView("_ConfirmDelete", viewModel);
        }

        [HttpPost]
        public IActionResult ConfirmDelete(ProductPackageDetailsViewModel productPackage, string currency = null)
        {
            _productPackageManager.DeleteProductPackage(productPackage.ProductPackageViewModel._id);
            return RedirectToAction("Index", new { currency });
        }

        #endregion

        #region Create

        [HttpGet]
        public IActionResult Create(string currency = null)
        {
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
            var viewModel = new EditProductPackageViewModel();
            var productModels = _productManager.GetProductModels();

            foreach(var product in productModels)
            {   
                var productViewModel= new ProductViewModel(product);
                productViewModel.Value = _currencyManager.ApplyExchangeRate(product.usdPrice, currency);            
                viewModel.AvailableProducts.Add(productViewModel);
            }
            
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Create(EditProductPackageViewModel viewModel, string currency = null)
        {
            if (!ModelState.IsValid)
            {
                _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);
                return View(viewModel);
            }
            var data = new ProductPackageModel()
            {
                Name = viewModel.Name,
                Description = viewModel.Description,
                Products = new List<ProductModel>()
            };


            if (viewModel.SelectedProductsIds != null && viewModel.SelectedProductsIds.Any())
            {
                foreach (var productIds in viewModel.SelectedProductsIds)
                {
                    data.Products.Add(new ProductModel() { Id = productIds });
                }
            }

            var result = _productPackageManager.CreateProductPackage(data);

            return RedirectToAction("details", new { id = result._id, currency });
        }

        #endregion

        #region Edit

        [HttpGet]
        public IActionResult Edit(string id, string currency=null)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index");
            }
            
            var productPackage = _productPackageManager.GetProductPackage(id);

            var viewModel = new EditProductPackageViewModel();
            viewModel.Name = productPackage.Name;
            viewModel.Description = productPackage.Description;
            viewModel.SelectedProductsIds = productPackage.Products.Select(p => p.Id).ToList();

            SetupEditView(viewModel, ViewBag, currency);
            return View(viewModel);
        }

        [HttpPost]
        public IActionResult Edit(string id, EditProductPackageViewModel viewModel, string currency = null)
        {
            if (!ModelState.IsValid)
            {
                SetupEditView(viewModel, ViewBag, currency);
                return View(viewModel);
            }

            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("Index");
            }

            var data = new ProductPackageModel()
            {
                _id = id,
                Name = viewModel.Name,
                Description = viewModel.Description,
                Products = new List<ProductModel>()
            };

            if (viewModel.SelectedProductsIds != null && viewModel.SelectedProductsIds.Any()) {
                foreach (var productIds in viewModel.SelectedProductsIds)
                {
                    data.Products.Add(new ProductModel() { Id = productIds });
                }
            }
            _productPackageManager.UpdateProductPackage(data);
            return RedirectToAction("details", new { id, currency });
        }

        private void SetupEditView(EditProductPackageViewModel viewModel, dynamic viewBag, string currency)
        {
            _controllerHelper.ConfigureCurrencyInViewBag(ViewBag, currency);

            var productModels = _productManager.GetProductModels();

            foreach (var product in productModels)
            {
                var productViewModel = new ProductViewModel(product);
                productViewModel.Value = _currencyManager.ApplyExchangeRate(product.usdPrice, currency);
                viewModel.AvailableProducts.Add(productViewModel);
            }
        }
        #endregion
    }
}