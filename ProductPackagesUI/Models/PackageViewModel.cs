﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductPackagesUI.Models
{
    public class ProductPackageViewModel
    {
        public ProductPackageViewModel(BusinessLayer.Managers.ProductPackage.ProductPackageModel productPackage)
        {
            if (productPackage != null)
            {
                _id = productPackage._id;
                Name = productPackage.Name;
                Description = productPackage.Description;
                Currency = productPackage.Currency;
                TotalValue = productPackage.TotalValue;
                Products = productPackage.Products.Select(product => new ProductViewModel(product)).ToList();
            }
        }

        public string _id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [Display(Name="Number of Products")]
        public IList<ProductViewModel> Products { get; set; }
        public string Currency { get; set; }

        [Display(Name = "Total Value")]
        public double TotalValue { get; set; } = 0;

    }
}
