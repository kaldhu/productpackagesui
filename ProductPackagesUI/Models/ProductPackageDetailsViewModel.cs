﻿using BusinessLayer;
using BusinessLayer.Managers.Currency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductPackagesUI.Models
{
    public class ProductPackageDetailsViewModel
    {
        public ProductPackageDetailsViewModel() 
        {
            ProductPackageViewModel = new ProductPackageViewModel(null);
        }

        public ProductPackageViewModel ProductPackageViewModel { get; set; }
    }
}
