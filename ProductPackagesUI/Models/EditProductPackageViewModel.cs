﻿using BusinessLayer;
using BusinessLayer.Managers.Currency;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductPackagesUI.Models
{
    public class EditProductPackageViewModel
    {

        public EditProductPackageViewModel()
        {
            SelectedProductsIds = new List<string>();
            AvailableProducts = new List<ProductViewModel>();
        }

        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Name { get; set; }

        [Required(AllowEmptyStrings =false)]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Description { get; set; }

        public IList<string> SelectedProductsIds { get; set; }
        [DisplayName("Products")]
        public IList<ProductViewModel> AvailableProducts { get; set; }
    }
}
