﻿using BusinessLayer;
using BusinessLayer.Managers.Link;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ProductPackagesUI.Models
{
    public class ProductViewModel
    {
        public ProductViewModel(BusinessLayer.Managers.Product.ProductModel product)
        {
            Id = product.Id;
            Name = product.Name;
            Value = product.usdPrice;
            Links = product.Links;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public LinkModel Links { get; set;}

    }
}
