This is hosted at https://productpackageswebapp.azurewebsites.net

Uses the "ProductPackages" service created by me, this is hosted at https://productpackages.azurewebsites.net.

uses 3rd party API "product-service" to get Product information, this is hosted at https://product-service.herokuapp.com/api/v1/products
uses 3rd party API "Exchange Rates API" to get Exchange rate information, this is hosted at https://api.exchangeratesapi.io/

To Host this locally, you will need a copy of the ProductPackages service running locally on a different port.
Change "_apiAddress" variable found in BusinessLayer/Managers/ProductPackage/ProductPackageManager.

I will move this to a config file later..