﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace BusinessLayer.Managers.Product
{
    public class ProductManager: IProductManager
    {
        private readonly IProcessWebRequest _processWebRequest;
        private readonly string _apiAddress = "https://product-service.herokuapp.com/api/v1/products";
        private readonly string _apiUserName = "user";
        private readonly string _apiPassword = "pass";

        public ProductManager(IProcessWebRequest processWebRequest)
        {
            _processWebRequest = processWebRequest;
        }

        public IEnumerable<ProductModel> GetProductModels()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_apiAddress);
            webRequest.Method = "GET";
            webRequest.Credentials = new NetworkCredential(_apiUserName, _apiPassword);
            var unProcessedProduct = _processWebRequest.GetWebResponse(webRequest);
            var productModels = JsonConvert.DeserializeObject<IList<ProductModel>>(unProcessedProduct);

            return productModels;
        }

        public ProductModel GetProductModel(string id)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{_apiAddress}/{id}");
            webRequest.Method = "GET";
            webRequest.Credentials = new NetworkCredential(_apiUserName, _apiPassword);
            var unProcessedProduct = _processWebRequest.GetWebResponse(webRequest);
            var productModel = JsonConvert.DeserializeObject<ProductModel>(unProcessedProduct);

            return productModel;
        }
    }
}
