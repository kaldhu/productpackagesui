﻿using BusinessLayer;
using BusinessLayer.Managers.Link;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BusinessLayer.Managers.Product
{
    public class ProductModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int usdPrice { get; set; }
        public LinkModel Links { get; set;}
        
    }
}
