﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Managers.Product
{
    public interface IProductManager
    {
        IEnumerable<ProductModel> GetProductModels();
        ProductModel GetProductModel(string id);
    }
}
