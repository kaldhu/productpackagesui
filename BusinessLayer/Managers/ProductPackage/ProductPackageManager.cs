﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace BusinessLayer.Managers.ProductPackage
{
    public class ProductPackageManager : IProductPackageManager
    {
        private readonly IProcessWebRequest _processWebRequest;
        private readonly string _apiAddress = "https://productpackages.azurewebsites.net/api/packages";


        //TODO: Caching productPackages

        public ProductPackageManager(IProcessWebRequest processWebRequest)
        {
            _processWebRequest = processWebRequest;

        }

        public IEnumerable<ProductPackageModel> GetAllProductPackages(string currency = null)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{_apiAddress}?currency={ currency}");
            webRequest.Method = "GET";
            var unprocessedData = _processWebRequest.GetWebResponse(webRequest);

            var productPackageModels = JsonConvert.DeserializeObject<IEnumerable<ProductPackageModel>>(unprocessedData);
            return productPackageModels;
        }

        public ProductPackageModel GetProductPackage(string id, string currency = null)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{_apiAddress}/{id}?currency={currency}");
            webRequest.Method = "GET";
            var unprocessedData = _processWebRequest.GetWebResponse(webRequest);
            var productPackageModel = JsonConvert.DeserializeObject<ProductPackageModel>(unprocessedData);
            return productPackageModel;
        }

        public void DeleteProductPackage(string id)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{_apiAddress}/{id}");
            webRequest.Method = "DELETE";
            _processWebRequest.GetWebResponse(webRequest);

            //TODO: Get Confirmation
        }

        public void UpdateProductPackage(ProductPackageModel productPackageModel)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create($"{_apiAddress}/{productPackageModel._id}");
            webRequest.Method = "PUT";
            webRequest.ContentType = "application/json";
            _processWebRequest.AddDataToRequest(webRequest, productPackageModel);
            _processWebRequest.GetWebResponse(webRequest);

            //TODO: Get Confirmation
        }

        public ProductPackageModel CreateProductPackage(ProductPackageModel productPackageModel)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_apiAddress);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            _processWebRequest.AddDataToRequest(webRequest, productPackageModel);

            var unprocessedData = _processWebRequest.GetWebResponse(webRequest);
            var result = JsonConvert.DeserializeObject<ProductPackageModel>(unprocessedData);

            //TODO: Get Confirmation
            return result;
        }
    }
}
