﻿
using System.Collections.Generic;
using BusinessLayer.Managers.Link;
using BusinessLayer.Managers.Product;

namespace BusinessLayer.Managers.ProductPackage
{
    public class ProductPackageModel
    {
        public string _id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public IList<ProductModel> Products { get; set; }
        public string Currency { get; set; }
        public double TotalValue { get; set; } 
        public LinkModel Links { get; set; }

    }
}
