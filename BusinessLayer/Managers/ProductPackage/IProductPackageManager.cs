﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayer.Managers.ProductPackage
{
    public interface IProductPackageManager
    {
        IEnumerable<ProductPackageModel> GetAllProductPackages(string currency = null);
        ProductPackageModel GetProductPackage(string id, string currency = null);
        void DeleteProductPackage(string id);
        void UpdateProductPackage(ProductPackageModel productPackageModel);
        ProductPackageModel CreateProductPackage(ProductPackageModel productPackageModel);
    }
}
