﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BusinessLayer.Managers.Currency
{
    public class ExchangeRates
    {
        public Dictionary<string, decimal> Rates { get; set; }
        public string Base { get; set; }
    }
}
