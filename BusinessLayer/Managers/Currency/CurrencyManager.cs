﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Newtonsoft.Json;

namespace BusinessLayer.Managers.Currency
{
    public class CurrencyManager : ICurrencyManager
    {
        private readonly IProcessWebRequest _processWebRequest;

        private readonly Dictionary<string, string> SymbolsByCode;
        private readonly string _defaultCurrency;
        private readonly string _currencyAPI;

        private Dictionary<string, decimal> _currentRates;
        private DateTime _currentRatesExpirary;

        public CurrencyManager(IProcessWebRequest processWebRequest,
            string currencyAPI,
            string defaultCurrency)
        {
            _processWebRequest = processWebRequest;
            _defaultCurrency = defaultCurrency;
            _currencyAPI = currencyAPI;
            SymbolsByCode = new Dictionary<string, string>();

            var regions = CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                          .Select(x => new RegionInfo(x.LCID));

            foreach (var region in regions)
                if (!SymbolsByCode.ContainsKey(region.ISOCurrencySymbol))
                    SymbolsByCode.Add(region.ISOCurrencySymbol, region.CurrencySymbol);
        }

        private void UpdateCurrencyRates()
        {
            if (_currentRates == null || _currentRatesExpirary < DateTime.UtcNow)
            {
                HttpWebRequest getPackageData = (HttpWebRequest)WebRequest.Create(_currencyAPI);
                getPackageData.Method = "GET";
                var unprocessedData = _processWebRequest.GetWebResponse(getPackageData);
                var exchangeRates = JsonConvert.DeserializeObject<ExchangeRates>(unprocessedData);
                _currentRates = exchangeRates.Rates;
                _currentRatesExpirary = DateTime.UtcNow;
            }
        }
        public string GetDefaultCurrency()
        {
            return _defaultCurrency;
        }
        public string GetSymbol(string code)
        {
            return SymbolsByCode[code];
        }

        public string[] GetAllAvailableCurrencies()
        {
            UpdateCurrencyRates();
            return _currentRates.Select(ex => ex.Key).ToArray();
        }

        public decimal ApplyExchangeRate(decimal amount, string currency)
        {
            UpdateCurrencyRates();
            if (!_currentRates.ContainsKey(currency))
            {
                currency = _defaultCurrency;
            }
            var exchangeRateToApply = _currentRates[currency];
            return Math.Round(amount * exchangeRateToApply, 2);
        }


    }
}
