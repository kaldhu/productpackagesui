﻿namespace BusinessLayer.Managers.Currency
{
    public interface ICurrencyManager
    {
        string GetDefaultCurrency();
        string GetSymbol(string code);
        string[] GetAllAvailableCurrencies();
        decimal ApplyExchangeRate(decimal amount, string currency);
    }
}