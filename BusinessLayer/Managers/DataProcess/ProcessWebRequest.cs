﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace BusinessLayer
{
    public class ProcessWebRequest : IProcessWebRequest
    {
        public void AddDataToRequest<T>(HttpWebRequest httpWebRequest, T data)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            httpWebRequest.ContentLength = jsonData.Length;
            using (var streamOut = new StreamWriter(httpWebRequest.GetRequestStream(), Encoding.ASCII))
            {
                streamOut.Write(jsonData);
            }
        }

        //TODO: Get Response code and content
        public string GetWebResponse(HttpWebRequest httpWebRequest)
        {
            if (httpWebRequest == null)
            {
                throw new ArgumentNullException();
            }
            string responseString = "";

            var response = httpWebRequest.GetResponse();
            
            using (var streamIn = new StreamReader(response.GetResponseStream()))
            {
                responseString = streamIn.ReadToEnd();
                return responseString;
            }
        }
    }
}
