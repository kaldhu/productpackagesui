﻿using System.Net;

namespace BusinessLayer
{
    public interface IProcessWebRequest
    {
        void AddDataToRequest<T>(HttpWebRequest httpWebRequest, T data);
        string GetWebResponse(HttpWebRequest httpWebRequest);
    }
}